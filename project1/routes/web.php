<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Frontend\MemberController;
use App\Http\Controllers\Frontend\BlogListController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\CheckOutController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
// frontend
Route::group([
	'prefix' => 'member',
],function(){
	Route::get('/register',[MemberController::class,'create']);
	Route::post('/register',[MemberController::class,'store']);
	// login
	Route::get('/login',[MemberController::class,'index']);
	Route::post('/login',[MemberController::class,'show']);
	// login
	Route::get('/login',[MemberController::class,'index']);
	Route::post('/login',[MemberController::class,'show']);
	// logout
	Route::get('/logout',[MemberController::class,'logout']);
	// account
	Route::get('/account',[MemberController::class,'edit'])->name('member');
	Route::post('/account',[MemberController::class,'update']);
	// list product
	Route::get('/product',[ProductController::class,'index'])->name('member');
	// add product
	Route::get('/addproduct',[ProductController::class,'create'])->name('member');
	Route::post('/addproduct',[ProductController::class,'store'])->name('member');
	Route::get('/editproduct/{id}',[ProductController::class,'edit'])->name('member');
	Route::post('/editproduct/{id}',[ProductController::class,'update'])->name('member');
	Route::get('/deleteproduct/{id}',[ProductController::class,'destroy'])->name('member');
	// product detail
	Route::get('/product/detail/{id}',[ProductController::class,'show']);

	// home
	Route::get('/home',[HomeController::class,'index']);
	Route::post('/home',[HomeController::class,'show']);

});
// ajax add to cart
Route::post('/home/product_ajax',[HomeController::class,'ajax_product']);
// blog-list
Route::get('/blog/list',[BlogListController::class,'index']);
// blog-detail
Route::get('/blog/detail/{id}',[BlogListController::class,'show']);
// xử lý ajax rate
Route::post('/blog/detail/rate_ajax',[BlogListController::class,'ajax_rate']);
// xu ly comment
Route::post('/blog/detail/cmt',[BlogListController::class,'comment']);
// cart
Route::get('/cart',[CartController::class,'index']);
// ajax cart up
Route::post('/cart_ajax',[CartController::class,'ajax_cart']);
// ajax cart down
Route::post('/cart_down',[CartController::class,'ajax_cart_down']);
// ajax cart delete
Route::post('/cart_delete',[CartController::class,'ajax_cart_delete']);
// checkout
Route::get('/checkout',[CheckOutController::class,'index'])->name('checkout');
Route::post('/checkout',[CheckOutController::class,'ajax_checkout'])->name('checkout');



Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// admin
Route::group([
	'prefix' => 'admin',
],function(){
	Route::get('/trangchu',[DashboardController::class,'index']);
	// trang profile
	Route::get('/profile',[UserController::class,'edit']);
	Route::post('/profile',[UserController::class,'update']);
	// country page
	Route::get('/country',[CountryController::class,'index']);
	Route::get('/add_country',[CountryController::class,'create']);
	Route::post('/add_country',[CountryController::class,'store']);
	Route::get('/edit_country/{id}',[CountryController::class,'edit']);
	Route::post('/edit_country/{id}',[CountryController::class,'update']);
	Route::get('/country/delete/{id}',[CountryController::class,'destroy']);

	// brand page
	Route::get('/brand',[BrandController::class,'index']);
	Route::get('/brand/add',[BrandController::class,'create']);
	Route::post('/brand/add',[BrandController::class,'store']);
	Route::get('/brand/delete/{id}',[BrandController::class,'destroy']);
	// category page
	Route::get('/category',[CategoryController::class,'index']);
	Route::get('/category/add',[CategoryController::class,'create']);
	Route::post('/category/add',[CategoryController::class,'store']);
	Route::get('/category/delete/{id}',[CategoryController::class,'destroy']);
	// blog page
	Route::get('/blog',[BlogController::class,'index']);
	Route::get('/blog/add',[BlogController::class,'create']);
	Route::post('/blog/add',[BlogController::class,'store']);
	Route::get('/blog/edit/{id}',[BlogController::class,'edit']);
	Route::post('/blog/edit/{id}',[BlogController::class,'update']);
	Route::get('/blog/delete/{id}',[BlogController::class,'destroy']);

});
