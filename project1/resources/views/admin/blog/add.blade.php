@extends("admin.layout.main")
	@section("content")

            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Create Blog</h4>
                    </div>
            
                </div>
            </div>
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                         @if(session('success'))
                          <div class="alert alert-danger alert-dismissible">
                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                             <h4><i class="icon fa fa-check"></i>Thông Báo!</h4>
                             {{session('success')}}
                          </div>
                        @endif
                         @if($errors->any())
                          <div class="alert alert-danger alert-dismissible">
                              
                              <ul>
                                  @foreach($errors->all() as $error)
                                      <li>{{$error}}</li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                        <div class="card card-body">
                            <form method="POST" enctype="multipart/form-data" class="form-horizontal m-t-30">
                                @csrf
                                <div class="form-group">
                                    
                                    <label>Title:</label>
                                    <input type="text" name="title"  class="form-control" value="">
                                    
                                </div>
                                    <label>Image:</label>
                                    <input type="file" name="avatar"  class="form-control" value="">
                                <div class="form-group">
                                        <label>Description:</label>
                                        <div class="">
                                            <textarea name="description" rows="5" class="form-control form-control-line"></textarea>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label>Content:</label>
                                        <div class="">
                                            <textarea name="content" id="demo" rows="5" class="form-control form-control-line">
                                                
                                            </textarea>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success">Create Blog</button>
                                </div>
                            </div>
                               
                            </form>
                             
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Nice admin. Designed and Developed by
                <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
            <script>CKEDITOR.replace( 'demo', {
                filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
                filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
                filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
                filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
                filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
                filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
            } );
 </script>
@endsection