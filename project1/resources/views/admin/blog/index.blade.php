@extends("admin.layout.main")
	@section("content")
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="table-responsive cart_info">
                    @if(session('success'))
                                  <div class="alert alert-danger alert-dismissible">
                                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                     <h4><i class="icon fa fa-check"></i>Thông Báo!</h4>
                                     {{session('success')}}
                                  </div>
                            @endif
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu" style="background:#dee2e6;font-size: 16px;font-weight: 500;">
                                <td class="image" style="width:5%;">Id</td>
                                <td class="description" style="width: 25%;">Title</td>
                                <td class="description" style="width: 25%;">Image</td>
                                <td class="description" style="width: 28%;">Description</td>
                                <td class="description">Action</td>
                                <td class="description"></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                // echo "<pre>";
                                // var_dump($data);
                                foreach ($data as $key => $value) {
                                    
                            ?>


                            <tr style="background: white">
                                <td class="cart_product"><?php echo $value['id']?></td>
                                <td class="cart_description"><?php echo $value['title']?></td>
                                <td class="cart_description"><?php echo $value['image']?></td>
                                <td class="cart_product"><?php echo $value['description']?></td>
                                <td>
                                <a class="cart_quantity_delete" href="{{url('admin/blog/edit/'.$value['id'])}}"><i class="mdi mdi-account-edit"></i>
                                        Edit
                                    </a>
                                   
                                </td>
                                
                    
                                <td class="cart_delete">
                                   <a class="cart_quantity_delete" href="{{url('admin/blog/delete/'.$value['id'])}}"><i class="mdi mdi-delete"></i>
                                    Delete
                                    </a>
                                </td>
                            
                            </tr>

                           
                           <?php } ?>
                        </tbody>
                    </table>    
                    <tfoot>
                        <tr>
                            <td colspan="8">
                                <a href="/admin/blog/add">
                                    <button style="margin: 2px;margin-right: 10px;float: right;background: #5ac146;color: white;border: none;width: 7%;">Add Blog
                                    </button>
                                </a>
                            </td>
                        </tr>
                    </tfoot>
                   
                    {{$data->links('pagination::bootstrap-4')}}
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Nice admin. Designed and Developed by
                <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
@endsection