@extends("admin.layout.main")
	@section("content")
            
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                         @if($errors->any())
                          <div class="alert alert-danger alert-dismissible">
                              
                              <ul>
                                  @foreach($errors->all() as $error)
                                      <li>{{$error}}</li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                        <div class="card card-body">
                            <form method="POST" class="form-horizontal m-t-30">
                                @csrf
                                 <?php foreach ($data as $key => $value) {?>
                                <div class="form-group">

                                    <label>Name:</label>
                            <input type="text" name="name"  class="form-control" value="<?php echo $value['name']?>">
                                    
                                </div>
                                <?php } ?>
                                <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success">Edit</button>
                                        </div>
                            </div>
                               
                            </form>
                             
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <footer class="footer text-center">
                All Rights Reserved by Nice admin. Designed and Developed by
                <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
@endsection