@extends("frontend.layout.main")
@section("content")
	<div class="col-lg-8 col-sm-offset-1">
					@if($errors->any())
			          <div class="alert alert-danger alert-dismissible">
			              
			              <ul>
			                  @foreach($errors->all() as $error)
			                      <li>{{$error}}</li>
			                  @endforeach
			              </ul>
			          </div>
					@endif
					@if(session('success'))
                          <div class="alert alert-danger alert-dismissible" style="background: palegreen">
                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                             <h4><i class="icon fa fa-check"></i>Thông Báo!</h4>
                             {{session('success')}}
                          </div>
 					@endif
					<div class="login-form"><!--login form-->
						<h2>Create Product!</h2>
						
						<form method="POST" action="" enctype="multipart/form-data">
							@csrf
							<input type="text" name="name" placeholder="Name"  />
							<input type="text" name="price" placeholder="Price"  />
							
							<select name='category'>
								<option>Please choose brand</option>
								@foreach($getBrand as $key=>$value)
				                <option value="{{$value['id']}}">{{$value['name']}}</option>
				                @endforeach
				            </select>
				           
							<select name='brand' style="margin-top: 10px;margin-bottom: 10px;">
				                <option>Please choose category</option>		   
				                @foreach($getCategory as $key => $value)
				                <option value="{{$value
				                ['id']}}">{{$value['name']}}</option>
				                @endforeach
				               
				            </select>
							<select id="status"  style="margin-top: 5px;margin-bottom: 10px;"  name="status">
								<option value="0">New</option>
								<option value="1">Sale</option>
								
							</select>
							
			                <input  id="sale" style="width: 25%;display: none" type="text" name="sale" value="0"  /> 
			                
							<input type="text" name="company" placeholder="Company profile"   />
							
							<input type="file" name="image[]" multiple />
							
							<textarea name="message" rows="11"></textarea>
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/login form-->
	</div>

	<script>
    	
    	$(document).ready(function(){
    		$("select#status").change(function(){
				var values=$("select#status").val();
				// console.log(values);
				
				if(values==1){
					 $("select#status").closest('form').find('input#sale').show();
				}else{
					 $("select#status").closest('form').find('input#sale').hide();
					
				}
				
			});
			
			
		   				
		    
		});
    </script>

@endsection