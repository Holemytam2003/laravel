@extends("frontend.layout.main")
@section("content")
	<div class="col-lg-8 col-sm-offset-1">
					@if($errors->any())
			          <div class="alert alert-danger alert-dismissible">
			              
			              <ul>
			                  @foreach($errors->all() as $error)
			                      <li>{{$error}}</li>
			                  @endforeach
			              </ul>
			          </div>
					@endif
					@if(session('success'))
                          <div class="alert alert-danger alert-dismissible" style="background: palegreen">
                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                             <h4><i class="icon fa fa-check"></i>Thông Báo!</h4>
                             {{session('success')}}
                          </div>
 					@endif
					<div class="login-form"><!--login form-->
						<h2>Edit Product!</h2>
						
						<form method="POST" action="" enctype="multipart/form-data">
							@csrf
							<input type="text" name="name" value="{{$getData['name']}}" placeholder="Name"  />
							<input type="text" name="price" value="{{$getData['price']}}" placeholder="Price"  />
							
							<select name='id_brand'>
								<?php foreach ($getBrand as $key => $value) {?>
				                <option value="{{$value['id']}}" 
				      {{$getData['id_brand'] == $value['id'] ? 'selected' : '' }}
				                >
				                	{{$value['name']}}
				                </option>
				                <?php } ?>
				            </select>
				           
							<select name='id_category' style="margin-top: 10px;margin-bottom: 10px;">
				                
				               @foreach ($getCategory as $key => $value)
				                <option value="{{$value['id']}}"
				                {{
				    			$getData['id_category'] == $value['id'] ? 'selected' : ''
				    			}}
				                >
				                	{{$value['name']}}
				                </option>
				                @endforeach 
				                
				               
				            </select>
							<select id="status"  style="margin-top: 5px;margin-bottom: 10px;"  name="status">

								<?php 
								 $name = "new";
								 for ($i=0; $i < 2; $i++) { 
								 	if($i==1){
								 		$name = "sale";
								 	}
								?>
									<option value="{{$i}}" 
							{{ $getData['status'] == $i ? 'selected' : ''}}>
									{{$name}}
										
									</option>
									
								<?php 
								 }
								?>
								
								
							</select>
							
							
							@if($getData['sale'] > 0)
			                	<input  id="sale"  style="width: 25%;" type="text" name="sale" value="{{$getData['sale']}}" /> 
			                @else
			               		 <input  id="sale"  style="width: 25%;display:none" type="text" name="sale" value="0" /> 
			            	@endif
			                
							<input type="text" name="company" value="{{$getData['company']}}" placeholder="Company profile"   />
							 
							<input type="file" name="image[]" multiple />
							<?php
							$getArrImage = json_decode($getData['image'],true);
							?>
							<ul class="image">
								@foreach ($getArrImage as $key => $value)
								
								<li>
									<img style="width: 150px" src="{{asset('/frontend/upload/product/'.$value)}}">

									<input name="hinhxoa[]" type="checkbox" value="{{$value}}"/>
								</li>
								@endforeach
							</ul>
							
							<textarea name="detail"  rows="11">
							{{$getData['detail']}}
							</textarea>
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/login form-->
	</div>
	<script>
    	
    	$(document).ready(function(){
    		$("select#status").change(function(){
				var values=$("select#status").val();
				console.log(values);
				
				if(values==1){
					 $('input#sale').show();
				}else{
					 $('input#sale').val(0).hide();
					 
				}
				
			});
			
			
		   				
		    
		});
    </script>
@endsection