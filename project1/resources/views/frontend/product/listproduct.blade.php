@extends("frontend.layout.main")
@section("content")

	<section id="cart_items">
		@if(session('success'))
                          <div class="alert alert-danger alert-dismissible" style="background: palegreen">
                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                             <h4><i class="icon fa fa-check"></i>Thông Báo!</h4>
                             {{session('success')}}
                          </div>
 			@endif
 			@if($errors->any())
	          <div class="alert alert-danger alert-dismissible">
	              
	              <ul>
	                  @foreach($errors->all() as $error)
	                      <li>{{$error}}</li>
	                  @endforeach
	              </ul>
	          </div>
		@endif
		<div class="table-responsive cart_info">
					<table class="table table-condensed">
						<thead>
							<tr class="cart_menu">
								<td class="image">Id</td>
								<td class="description" style="width: 20%;">Name</td>
								<td class="price" style="width: 19%">Image</td>
								<td class="quantity">Price</td>
								<td class="total">Action</td>
								<td></td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							
							@foreach ($getData as $key => $value)
							<?php
								$getArrImage = json_decode($value['image'],true);
							?>
							<tr>
								<td class="cart_product" style="font-size: 20px;">
									<p>{{$value['id']}}</p>
								</td>
								<td class="cart_description">
									<h4><a href="">{{$value['name']}}</a></h4>
									
								</td>
								<td class="cart_price">
									<a href=""><img style="width: 100%;" src="{{asset('/frontend/upload/product/'.$getArrImage[0])}}" alt=""></a>
								</td>
								<td class="cart_quantity" style="font-size: 20px;">
									<p>{{$value['price']}}</p>
								</td>
								<td class="">
									<a class="" href="{{url('/member/editproduct/'.$value['id'])}}"><i class="fa fa-pencil-square-o"  style="font-size:30px;color:#428bca"></i></a>
								</td>
								<td class="">
									<a class="" href="{{url('/member/deleteproduct/'.$value['id'])}}"><i class="fa fa-times"  style="font-size:30px;color:#428bca"></i></a>
								</td>
							</tr>
							@endforeach
						</tbody>
		
					</table>
					

		</div>
		<div class="pagination-area">
								{{$getData->links('pagination::bootstrap-4')}}
		</div>
		<button class="product">
			<a style="color:white" href="/member/addproduct">Add new</a>
		</button>
	</section>
@endsection