@extends("frontend.layout.main")
@section("content")
	
	<section id="cart_items">
		
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							$getSession = session()->get('cart');
							$total = 0 ;
							$tong = 0 ;

						?>
						@if(session()->has('cart'))
						@foreach($getSession as $key =>$value)
						<?php
							$getPrice = $value['price'];
							// echo $getPrice;
							$getQty = $value['qty'];
							// echo $getQty;

							$deletePrice = explode('$', $getPrice);
							// echo "<pre>";
							
							$total = $getQty * $deletePrice[1];
							// echo $total;
							$tong = $tong + $total;
							// echo $tong;
							
						?>
						<tr>
							<td class="cart_product">
								<a href=""><img src="{{asset('frontend/upload/product/hinh85_'.$value['image'])}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$value['name']}}</a></h4>
								<p class="id">{{$value['id']}}</p>
							</td>
							<td class="cart_price">
								<p class="price">{{$value['price']}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up"> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="{{$value['qty']}}" autocomplete="off" size="2">
									<a class="cart_quantity_down"> - </a>
								</div>
							</td>
							
							<td class="cart_total">
								<p class="cart_total_price">${{$total}}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete"><i class="fa fa-times"></i></a>
							</td>
						</tr>
						@endforeach
						@endif
						<!-- <tr>
							<td class="cart_product">
								<a href=""><img src="images/cart/two.png" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">Colorblock Scuba</a></h4>
								<p>Web ID: 1089772</p>
							</td>
							<td class="cart_price">
								<p>$59</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$59</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr>
						<tr>
							<td class="cart_product">
								<a href=""><img src="images/cart/three.png" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">Colorblock Scuba</a></h4>
								<p>Web ID: 1089772</p>
							</td>
							<td class="cart_price">
								<p>$59</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$59</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr> -->
					</tbody>
				</table>
			</div>
		
	</section> <!--/#cart_items-->

		<section id="do_action">
		
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>$59</span></li>
							<li>Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total 
								<span class="total">${{$tong}}</span>
							</li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="{{url('/checkout')}}" >Check Out</a>
					</div>
				</div>
			</div>
		
	</section><!--/#do_action-->
	<script type="text/javascript">
		$(document).ready(function(){
			$('a.cart_quantity_up').click(function(){
				var getQty=$(this).closest('div.cart_quantity_button').find('input.cart_quantity_input').val();
				
				newQty = parseInt(getQty) + 1;
				$(this).closest('div.cart_quantity_button').find('input.cart_quantity_input').val(newQty);
				// ------------------------------------------------------
				var getPrice= $(this).closest('tr').find('p.price').text();
				
				var deletePrice = getPrice.replace('$','');
				
				newPrice = deletePrice * newQty;
				$(this).closest('tr').find('p.cart_total_price').text("$"+newPrice)
				// ------------------------------------------------------
				var getTotal = $('span.total').text();
				
				var deletePriceTotal = getTotal.replace('$','')
				
				newTotal = parseInt(deletePriceTotal) + parseInt(deletePrice)
				$('span.total').text("$" + newTotal)
				// ------------------------------------------------------
				var getId = $(this).closest('tr').find('p.id').text()
				// console.log(getId)
				$.ajax({
	                    type: "POST",
	                    url: "{{ url('/cart_ajax') }}",
	                    
	                    data: {
	                        
	                    	'getId': getId,
	                        
	                        "_token": "{{ csrf_token() }}"
	                    },
	                    success : function(response){
	                    	$("a.cart").html("<i class='fa fa-shopping-cart'></i>" + response.tongQty)
							
						}
                	});
			})
			$('a.cart_quantity_down').click(function(){
				var getQty = $(this).closest('div.cart_quantity_button').find('input.cart_quantity_input').val();
				// console.log(getQty);
				var getPrice = $(this).closest('tr').find('p.price').text();
				// console.log(getPrice)
				var deletePrice = getPrice.replace('$','')
				// console.log(deletePrice)
				var getTotal = $('span.total').text();
				// console.log(getTotal)
				var deletePriceTotal = getTotal.replace('$','')
				// console.log(deletePriceTotal)
				var getId = $(this).closest('tr').find('p.id').text();
				// console.log(getId)
				if(getQty>1){
					newQty = parseInt(getQty) -1;
					$(this).closest('div.cart_quantity_button').find('input.cart_quantity_input').val(newQty);

					newPrice = deletePrice * newQty;
					$(this).closest('tr').find('p.cart_total_price').text("$" + newPrice)
					newTotal = parseInt(deletePriceTotal) - parseInt(deletePrice)
					$('span.total').text("$" + newTotal)
				}else{
					$(this).closest('tr').remove();
					newTotal = parseInt(deletePriceTotal) - parseInt(deletePrice)
					$('span.total').text("$" + newTotal)
				}

				$.ajax({
	                    type: "POST",
	                    url: "{{ url('/cart_down') }}",
	                    
	                    data: {
	                        'getQty' : getQty,
	                    	'getId': getId,
	                        
	                        "_token": "{{ csrf_token() }}"
	                    },
	                    success : function(response){
	                    	$("a.cart").html("<i class='fa fa-shopping-cart'></i>" + response.tongQty)
							
						}
                	});

			})
			$('a.cart_quantity_delete').click(function(){
				$(this).closest('tr').remove();
				var getId = $(this).closest('tr').find('p.id').text();
				// console.log(getId)
				var getPrice = $(this).closest('tr').find('p.price').text();
				// console.log(getPrice);
				var price = getPrice.replace('$','')
				// console.log(price)
				var getTotal = $('span.total').text()
				// console.log(getTotal)
				var total = getTotal.replace('$','')
				newTotal = parseInt(total) - parseInt(price)
				$('span.total').text("$" + newTotal)
				$.ajax({
	                    type: "POST",
	                    url: "{{ url('/cart_delete') }}",
	                    
	                    data: {
	    
	                    	'getId': getId,
	                        
	                        "_token": "{{ csrf_token() }}"
	                    },
	                    success : function(response){
	                    	// console.log(response);
							
						}
                	});
			})
			
		})

	</script>
@endsection