@extends("frontend.layout.main")
@section("content")
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<!-- <div class="step-one">
				<h2 class="heading">Step1</h2>
			</div> -->
			<!--  <div class="checkout-options">

				<h3>New User</h3>
				<p>Checkout options</p>
				<ul class="nav">
					<li>
						<label><input type="checkbox"> Register Account</label>
					</li>
					<li>
						<label><input type="checkbox"> Guest Checkout</label>
					</li>
					<li>
						<a href=""><i class="fa fa-times"></i>Cancel</a>
					</li>
				</ul>
			</div>  -->

			<!-- <div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div> --><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					<div class="col-lg-8">
					<div class="signup-form" style="text-align: center;"><!--sign up form-->
						  @if(session('success'))
                          <div class="alert alert-dismissible" style="background: palegreen">
                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                             <h4><i class="icon fa fa-check"></i>Thông Báo!</h4>
                             {{session('success')}}
                          </div>
                        @endif
						@if($errors->any())
						          <div class="alert alert-danger alert-dismissible">
						              
						              <ul>
						                  @foreach($errors->all() as $error)
						                      <li>{{$error}}</li>
						                  @endforeach
						              </ul>
						          </div>
						@endif
						<?php 
							if(!Auth::check()){
						?>
						<h2>New User Signup!</h2>
						<form method="POST" enctype= multipart/form-data  action="">
							@csrf
							<input type="text" name="name" placeholder="Name"/>
							<input type="email" name="email" placeholder="Email Address"/>
							<input type="password" name="password" placeholder="Password"/>
							<input type="text" name="phone" placeholder="Phone"/>
							<input type="text" name="address" placeholder="Address"/>
							<input type="file" name="avatar" placeholder="Avatar"/>
							<button style="float:right;" type="submit" class="btn btn-default">Signup</button>
						</form>
						<?php
							}
						?>
					</div><!--/sign up form-->
	</div>				
				</div>
			</div>
			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							$tong = 0;
							$total = 0;
						?>
						@if(session()->has('cart'))
						@foreach($getSession as $key => $value)
						<?php
							
							$getPrice = explode('$', $value['price']);
							$price = $getPrice[1];
							$getQty = $value['qty'];
							$tong = $getQty * $price;
							$total = $tong + $total ;
						?>
						<tr class="product">
							<td class="cart_product">
								<a href=""><img src="{{asset('/frontend/upload/product/hinh85_'.$value['image'])}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$value['name']}}</a></h4>
								<p>Web ID: {{$value['id']}}</p>
							</td>
							<td class="cart_price">
								<p class="price">{{$value['price']}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="{{$value['qty']}}" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">${{$tong}}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr>
						@endforeach
						@endif
<!-- 
						<tr>
							<td class="cart_product">
								<a href=""><img src="images/cart/two.png" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">Colorblock Scuba</a></h4>
								<p>Web ID: 1089772</p>
							</td>
							<td class="cart_price">
								<p>$59</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$59</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr>
						<tr>
							<td class="cart_product">
								<a href=""><img src="images/cart/three.png" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">Colorblock Scuba</a></h4>
								<p>Web ID: 1089772</p>
							</td>
							<td class="cart_price">
								<p>$59</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$59</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr> -->
						<tr>
							
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>$59</td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>$2</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td>
											<span class="total">${{$total}}</span>
										</td>
									</tr>
								</table>
								
									<button style="background: #FE980F;" type="submit" class="btn btn-default order">
										Order
									</button>
								
							</td>
						</tr>
					</tbody>

				</table>

			</div>

			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
	</section> <!--/#cart_items-->
	<script type="text/javascript">
		$(document).ready(function(){
			$('button.order').click(function(){
				var checkLogin = "{{Auth::check()}}";
				// console.log(checkLogin);
				var getPrice = $('span.total').text();
				// console.log(getPrice);
				if(checkLogin==""){
					alert('Please login to check out or Register to order')
				}else{
					
					$.ajax({
	                    type: "POST",
	                    url: "{{ url('/checkout') }}",
	                    
	                    data: {
	                        'getPrice': getPrice,
	                        
	                        "_token": "{{ csrf_token() }}"
	                    },
	                    success : function(response){
	                    	if(response.data==1){
	                    		alert('Checkout success');
	                    	}else{
	                    		 alert('Fail');
	                    	}
						}
                	});
				}
			})
		})
	</script>
@endsection