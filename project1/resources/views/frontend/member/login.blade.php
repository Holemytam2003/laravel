@extends("frontend.layout.main")
	@section("content")
	<div class="col-lg-8 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						  @if(session('success'))
                          <div class="alert alert-dismissible" style="background: palegreen">
                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                             <h4><i class="icon fa fa-check"></i>Thông Báo!</h4>
                             {{session('success')}}
                          </div>
                        @endif
						@if($errors->any())
						          <div class="alert alert-danger alert-dismissible">
						              
						              <ul>
						                  @foreach($errors->all() as $error)
						                      <li>{{$error}}</li>
						                  @endforeach
						              </ul>
						          </div>
						@endif
						<form method="POST" action="">
							@csrf
							<input type="email" name="email" placeholder="Email" />
							<input type="password" name="password" placeholder="Password" />
							<span>
								<input type="checkbox" class="checkbox"> 
								Keep me signed in
							</span>
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
	</div>
@endsection