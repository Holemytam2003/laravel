@extends("frontend.layout.main")
@section("content")
	<div class="col-lg-8">
					<div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						  @if(session('success'))
                          <div class="alert alert-dismissible" style="background: palegreen">
                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                             <h4><i class="icon fa fa-check"></i>Thông Báo!</h4>
                             {{session('success')}}
                          </div>
                        @endif
						@if($errors->any())
						          <div class="alert alert-danger alert-dismissible">
						              
						              <ul>
						                  @foreach($errors->all() as $error)
						                      <li>{{$error}}</li>
						                  @endforeach
						              </ul>
						          </div>
						@endif
						<form method="POST" enctype= multipart/form-data  action="">
							@csrf
							<input type="text" name="name" placeholder="Name"/>
							<input type="email" name="email" placeholder="Email Address"/>
							<input type="password" name="password" placeholder="Password"/>
							<input type="text" name="phone" placeholder="Phone"/>
							<input type="text" name="address" placeholder="Address"/>
							<input type="file" name="avatar" placeholder="Avatar"/>
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/sign up form-->
	</div>
@endsection