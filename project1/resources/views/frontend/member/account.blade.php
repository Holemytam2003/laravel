@extends("frontend.layout.main")
	@section("content")
		<div class="col-lg-8 col-sm-offset-1">
					@if($errors->any())
			          <div class="alert alert-danger alert-dismissible">
			              
			              <ul>
			                  @foreach($errors->all() as $error)
			                      <li>{{$error}}</li>
			                  @endforeach
			              </ul>
			          </div>
					@endif
					@if(session('success'))
                          <div class="alert alert-danger alert-dismissible" style="background: palegreen">
                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                             <h4><i class="icon fa fa-check"></i>Thông Báo!</h4>
                             {{session('success')}}
                          </div>
 					@endif
					<div class="login-form"><!--login form-->
						<h2>User Update!</h2>
						
						<form method="POST" action="" enctype="multipart/form-data">
							@csrf
							<input type="text" name="name" placeholder="Name" value="{{$getData['name']}}" />
							<input type="email" name="email" placeholder="Email" value="{{$getData['email']}}" />
							<input type="password" name="password" placeholder="Password"  />
							<input type="text" name="address" placeholder="Address" value="{{$getData['address']}}"  />
							<input type="number" name="phone" placeholder="Phone" value="{{$getData['phone']}}"/>
							<input type="file" name="avatar" />
							
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/login form-->
	</div>
@endsection