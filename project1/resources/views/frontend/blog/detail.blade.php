@extends("frontend.layout.main")
	@section("content")

			<div class="blog-post-area">
				
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3>Girls Pink T Shirt arrived in store</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i> Mac Doe</li>
									<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
									<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
								</ul>
								<!-- <span>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
								</span> -->
							</div>
							
							<a href="">
								<img src="{{url('/admin/upload/blog/avatar/'.$getData['image'])}}" alt="">
							</a>
							<p>{!! $getData['content'] !!}</p> <br>

							
				
							<div class="pager-area">
								<ul class="pager pull-right">

									@if ($previous)
									
										<li>

											<a href="{{url('/blog/detail/'.$previous->id)}}">Pre</a>
											
										</li>
									@endif

									@if($next)
										<li>
											<a href="{{url('/blog/detail/'.$next->id)}}">Next</a>
										</li>
									@endif
								</ul>
							</div>
						</div>
			</div><!--/blog-post-area-->
			
			<div class="rating-area">
						<!-- <ul class="ratings">
							<li class="rate-this">Rate this item:</li>
							<li>
								<i class="fa fa-star color"></i>
								<i class="fa fa-star color"></i>
								<i class="fa fa-star color"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</li>
							<li class="color">(6 votes)</li>
						</ul> -->
						<div class="rate">
							<div class="vote">
								@for ($i=1 ; $i<=5 ; $i++)
			                    <div class="star_{{$i}} ratings_stars
			                    	{{$i <= $tbc ? 'ratings_over' : ''}}

			                    ">
			                    	
			                    	<input value="{{$i}}" type="hidden">
			                    	
			                    </div>
			                    @endfor
			                    <!-- <div class="star_2 ratings_stars">
			                    	<input value="2" type="hidden">
			                    </div>
			                    <div class="star_3 ratings_stars"><input value="3" type="hidden"></div>
			                    <div class="star_4 ratings_stars"><input value="4" type="hidden"></div>
			                    <div class="star_5 ratings_stars"><input value="5" type="hidden"></div> -->
			                    <span class="rate-np">{{$tbc}}</span>
			                   
			                </div>
			                
			            </div>
						<ul class="tag">
							<li>TAG:</li>
							<li><a class="color" href="">Pink <span>/</span></a></li>
							<li><a class="color" href="">T-Shirt <span>/</span></a></li>
							<li><a class="color" href="">Girls</a></li>
						</ul>
			</div><!--/rating-area-->

			<div class="socials-share">
						<a href=""><img src="{{asset('frontend/images/blog/socials.png')}}" alt=""></a>
			</div><!--/socials-share-->
			<div class="response-area">
						<h2>3 RESPONSES</h2>
						<ul class="media-list">
						
					
							@foreach($dataCmt as $key => $value)
								@if($value['level']==0)
							
									<li class="media">
										
										<a class="pull-left" href="#">
											<img class="media-object" src="{{url('/frontend/upload/user/avatar/'.$value['avatar'])}}" alt="">
										</a>
										<div class="media-body">
											<ul class="sinlge-post-meta">
												<li><i class="fa fa-user"></i>{{$value['name']}}</li>
												<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
												<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
											</ul>
											<p>{{$value['cmt']}}</p>
											<a class="btn btn-primary getId" id="{{$value['id']}}" href="#traloi">
												
												<i class="fa fa-reply" ></i>Replay
											</a>
										</div>
									</li>
									
								@endif
								@foreach($dataCmt as $key => $value_2)
									<!-- level con = id cha -->
									@if($value_2['level'] == $value['id'])
										<li class="media second-media">
											<a class="pull-left" href="#">
												<img  class="media-object" src="{{url('/frontend/upload/user/avatar/'.$value_2['avatar'])}}" alt="">
											</a>
											<div class="media-body">
												<ul class="sinlge-post-meta">
													<li><i class="fa fa-user"></i>{{$value_2['name']}}</li>
													<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
													<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
												</ul>
												<p>{{$value_2['cmt']}}</p>
												<a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
											</div>
										</li>
									@endif
								@endforeach
							@endforeach
						
						</ul>	
				<div class="replay-box">
						<div class="row">
							<div class="col-sm-12">
								<h2>Leave a replay</h2>
								@if(session('success'))
	                          <div class="alert alert-danger alert-dismissible" style="background: palegreen">
	                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
	                             <h4><i class="icon fa fa-check"></i>Thông Báo!</h4>
	                             {{session('success')}}
	                          </div>
								@endif
								<form action="{{url('/blog/detail/cmt')}}" method="POST">
								@csrf
								<input type="hidden" name="id" value="{{$getData['id']}}">

								<input type="hidden" class="level" name="level" value=0>
								<div class="text-area">
									
				                  	<p class="err_cmt" style="font-size:100%;
    						color: red;"></p>
									<div class="blank-arrow">
										<label>Your Name</label>
									</div>
									<span>*</span>
									<textarea id="traloi" name="message" rows="11"></textarea>
									
									<button type="submit"  class="btn btn-primary" >post comment</button>
									
								</div>
								<form/>
							</div>
						</div>
					
				</div><!--/Repaly Box-->
			</div>					
			</div><!--/Response-area-->
			<!-- <div class="replay-box">
						<div class="row">
							<div class="col-sm-12">
								<h2>Leave a replay</h2>
								@if(session('success'))
	                          <div class="alert alert-danger alert-dismissible" style="background: palegreen">
	                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
	                             <h4><i class="icon fa fa-check"></i>Thông Báo!</h4>
	                             {{session('success')}}
	                          </div>
								@endif
								<form action="{{url('/blog/detail/cmt')}}" method="POST">
								@csrf
								<input type="hidden" name="id" value="{{$getData['id']}}">

								<input type="hidden" name="level" value=0>
								<div class="text-area">
									
				                  	<p class="err_cmt" style="font-size:100%;
    						color: red;"></p>
									<div class="blank-arrow">
										<label>Your Name</label>
									</div>
									<span>*</span>
									<textarea name="message" rows="11"></textarea>
									
									<button type="submit"  class="btn btn-primary" >post comment</button>
									
								</div>
								<form/>
							</div>
						</div>
					</div><Repaly Box-->
			<!-- </div>	 --> 
    
     
   
    <script>
    	
    	$(document).ready(function(){
			//vote
			$('.ratings_stars').hover(
	            // Handles the mouseover
	            function() {
	                $(this).prevAll().andSelf().addClass('ratings_hover');
	                // $(this).nextAll().removeClass('ratings_vote'); 
	            },
	            function() {
	                $(this).prevAll().andSelf().removeClass('ratings_hover');
	                // set_votes($(this).parent());
	            }
	        );

			$('.ratings_stars').click(function(){
				var value =  $(this).find("input").val();
		        // console.log(Values);

		        var checkLogin = "{{Auth::check()}}";
		        // console.log(checkLogin);

		        var id_blog = "{{$getData['id']}}"
		        // console.log(getId);
		        // var id_user = "{{auth::id()}}"
		        // console.log(id_user);
		        
		        // console.log(tbc)
		        if(checkLogin==""){
		        	alert('Please login to rate');
		        }else{

		        	if ($(this).hasClass('ratings_over')) {
			            $('.ratings_stars').removeClass('ratings_over');
			            $(this).prevAll().andSelf().addClass('ratings_over');
			        } else {
			        	$(this).prevAll().andSelf().addClass('ratings_over');
			        }
			    
			        $.ajax({
	                    type: "POST",
	                    url: "{{ url('/blog/detail/rate_ajax') }}",
	                    
	                    data: {
	                        'value': value,
	                        'id_blog': id_blog,
	                        
	                        "_token": "{{ csrf_token() }}"
	                    },
	                    success : function(response){
	                    	
							if(response.data==1){
								alert('Rate success');
							}else{
								alert('You have rated the article');
							}
						}
                	});

		    	}
		    });
		    
		    // cmt
		    $('form').submit(function(){
		    	var checkLogin = "{{Auth::check()}}";
		    	var getCmt = $("textarea").val();
		    	// console.log(getCmt);
		    	if (checkLogin=="") {
		    		alert ('Please login to comment')
		    	}else{
		    		if(getCmt==""){
		    			$('p.err_cmt').html("Please input comment")
		    		}else{
		    			return true;
		    		}
		    	}

		    	 // dung form
		    	return false;
		    })
		    $('a.getId').click(function(){
		    	var getId = $(this).attr('id');
		    	// console.log(getId);
		    	var newLevel = $(this).closest('div.response-area').find('input.level').val(getId);
		    });
		   				
		    
		});
    </script>
@endsection