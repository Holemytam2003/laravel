<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table='products';
     protected $fillable = [
        'name',
        'price',
        'id_user',
        'id_brand',
        'id_category',
        'status',
        'sale',
        'company',
        'image',
        'detail',
        
    ];
    
}
