<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
       
        return view ('frontend/cart/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function ajax_cart(Request $request)
    {
        // dd($_POST['getId']);
        $getId = $_POST['getId'];
        if(session()->has('cart')){
            $getSession = session()->get('cart');
            $sum =0;
            // echo "<pre>";
            // var_dump($getSession);
            foreach ($getSession as $key => $value) {
                if($getId==$value['id']){
                    $getSession[$key]['qty'] +=1;
                    session()->put('cart',$getSession);
                }
                $sum = $sum + $getSession[$key]['qty'];
                
            }
           // echo($sum);
            return response()->json([
                'tongQty' => $sum
            ]);
            
            
        }
        // echo "<pre>";
        // var_dump(session()->get('cart'));
    }
     public function ajax_cart_down(Request $request)
    {
        $getId = $_POST['getId'];
        // echo $getId;
        $getQty = $_POST['getQty'];
        // echo $getQty;
      
        if(session()->has('cart')){
            $getSession = session()->get('cart');
            $sum = 0;
            if($getQty>1){
                foreach ($getSession as $key => $value) {
                    if($getId==$value['id']){
                        $getSession[$key]['qty'] -=1;
                        session()->put('cart',$getSession);
                    }
                    $sum = $sum + $getSession[$key]['qty'];
                }
               
            }else{
                foreach ($getSession as $key => $value) {
                   if($getId==$value['id']){
                        unset($getSession[$key]);
                        session()->put('cart',$getSession);
                   }
                    // $sum = $sum + $getSession[$key]['qty'];
                }
               
                
            }
            echo $sum;
            // echo $sum;
            return response()->json([
                    'tongQty' => $sum
            ]);
            

        }

    }
     public function ajax_cart_delete(Request $request)
    {
        $getId = $_POST['getId'];
        // echo $getId;
        if(session()->has('cart')){
            $getSession= session()->get('cart');
            foreach ($getSession as $key => $value) {
               if($getId==$value['id']){
                    unset($getSession[$key]);
                    session()->put('cart',$getSession);
               }
            }
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
