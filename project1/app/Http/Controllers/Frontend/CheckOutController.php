<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\History;
class CheckOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $getSession = session()->get('cart');
        // dd($getSession);
        
        return view ('frontend/checkout/checkout',compact('getSession'));
    }
    public function ajax_checkout(Request $request)
    {
       $id_user = Auth::id();
       $getDataUser = User::where('id',$id_user)->get()->toArray();
       // echo "<pre>";
       // var_dump($getDataUser[0]['name']);
      
       $data = new History;
       $data->email = $getDataUser[0]['email'];
       $data->name = $getDataUser[0]['name'];
       $data->id_user = $id_user;
       $data->price = $_POST['getPrice'];

       if($data->save()){
            session()->forget('cart');
            return response()->json(['data'=>1]);
       }else{
           return response()->json(['data'=>2]);
       }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $checkLogin = Auth::check();
        // echo 1111;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
