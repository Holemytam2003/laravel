<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AddProductRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Image;
use Auth;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = Product::paginate(3);
        // dd($getData);


        return view('frontend/product/listproduct',compact('getData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getBrand = Brand::all()->toArray();
        // dd($getBrand);
        $getCategory = Category::all()->toArray();
        // dd($getCategory);
        return view ('frontend/product/addproduct',compact('getBrand','getCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( AddProductRequest $request)
    {   
        $data = [];
        $id_user = Auth::id();
        if($request->hasfile('image'))
        {

            foreach($request->file('image') as $image){

                $name = $image->getClientOriginalName();
                $name_2 = "hinh85_".$image->getClientOriginalName();
                $name_3 = "hinh329_".$image->getClientOriginalName();

                // $image->move('upload/product/', $name);

                $path = public_path('frontend/upload/product/' . $name);
                $path2 = public_path('frontend/upload/product/' . $name_2);
                $path3 = public_path('frontend/upload/product/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(50,70)->save($path2);
                Image::make($image->getRealPath())->resize(200, 300)->save($path3);

                $data[] = $name;
            }
            
          
        };

        if(count($data)<=3){
            $product= new Product();
            $product->image = json_encode($data);
            $product->name = $request->name;
            $product->price = $request->price;
            $product->id_user = $id_user;
            $product->id_brand = $request->brand;
            $product->id_category = $request->category;
            $product->status = $request->status;
            $product->sale = $request->sale;
            $product->company = $request->company;
            $product->detail = $request->message;
            if($product->save()){
                 return redirect('member/product')->with('success','Add product success');
            }else{
                return redirect()->back()->withErrors('Add product failed');
            }
        }else{
            return redirect()->back()->withErrors('Only allow to upload 3 images');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getData = Product::where('id',$id)->get()->toArray();
        // dd($getData);
        $getArrImg = json_decode($getData[0]['image'],true);


        return view('frontend/product/detailproduct',compact('getArrImg'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)

    {
        $data = Product::where('id',$id)->get()->toArray();
        $getData = $data[0];
        $getBrand = Brand::all()->toArray();
        $getCategory = Category::all()->toArray();

        return view('frontend/product/editproduct',compact('getData','getBrand','getCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $img=[];
        $dataUpdate = $request->all();

        $product= Product::findOrFail($id);
        $id_user= Auth::id();
       
        
        $data = Product::where('id',$id)->get()->toArray();

        // - lay hinh anh cu ra 
        $oldImg = json_decode($data[0]['image'],true);

        // - lay hinh xoa ra
        $deleteImg = $request->hinhxoa;
        
        // - ktra co xoa hay kh:
        if($deleteImg){
            foreach ($oldImg as $key => $value) {
                   if(in_array($value, $deleteImg)){
                        unset($oldImg[$key]);

                   }
                }
                        // =>resetlai key
            $hinhconlai=array_values($oldImg);
            // dd($newImg);
        }

        // - hinh upload + hinh con lai > 3 => loi 
        // - < 3: 
        //     + xu ly :... => img
        //     + lay hinh upload len + merge voi hinh con lai 
        //     [1,2] merge [3] => 1,2,3 
        if($request->hasfile('image')){
            if(count($request->file('image')) + count($hinhconlai) <= 3){
                foreach($request->file('image') as $image){

                    $name = $image->getClientOriginalName();
                    $name_2 = "hinh85_".$image->getClientOriginalName();
                    $name_3 = "hinh329_".$image->getClientOriginalName();

                    // $image->move('upload/product/', $name);

                    $path = public_path('frontend/upload/product/' . $name);
                    $path2 = public_path('frontend/upload/product/' . $name_2);
                    $path3 = public_path('frontend/upload/product/' . $name_3);

                    Image::make($image->getRealPath())->save($path);
                    Image::make($image->getRealPath())->resize(50,70)->save($path2);
                    Image::make($image->getRealPath())->resize(200, 300)->save($path3);

                    $img[] = $name;
                }
                        // =>gop 2 mang lai thanh 1
                $newImg = array_merge($img,$hinhconlai);


            }else{
                return redirect()->back()->withErrors('Only allow to upload 3 images');
            }
            

        }
        if($request->image){
            $dataUpdate['image'] = json_encode($newImg);
        }
        if($request->id_user){
            $dataUpdate['id_user'] =  $id_user;
        }
        // dd($dataUpdate);

        if($product->update($dataUpdate)){
            
            return redirect('member/product')->with('success','Update product success');
        } else{
            return redirect()->back()->withErrors('Update product failed');
        }
       


        // - lay hinh anh cu ra 
        // - lay hinh xoa ra (co xoa k):
        // => xu ly 2 cai nay de lay hinh con lai 

        // - hinh upload + hinh con lai > 3 => loi 
        // - < 3: 
        //     + xu ly :... => img
        //     + lay hinh upload len + merge voi hinh con lai 
        //     [1,2] merge [3] => 1,2,3 

        // -- cac thong tin input khac
        //   update vao

       
        

        // $hinhcu = [1,2,3]
        // $hinhxoa = [1,2]
        //     foreach ($hinhcu as $key => $value) {
        //         if(in_array($value, $hinhxoa)){
        //             unset($hinhcu[$value])
        //         }
        //     }

        // => hinhconlai [3]

        


       
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id',$id)->delete();
        return redirect('member/product')->with('success','Delete category success');
    }
}
