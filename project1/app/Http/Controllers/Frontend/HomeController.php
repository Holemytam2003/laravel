<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // session()->forget('cart');
    

        $getProduct = Product::orderBy('created_at','ASC')
                    ->take(6)
                    ->get()
                    ->toArray();
        // dd($getProduct);
        return view ('frontend/home',compact('getProduct'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

     public function ajax_product(Request $request)
    {
       $id_product = $request->id_product;

       $dataProduct = Product::where('id',$id_product)->get()->toArray();

       $getArrImg = json_decode($dataProduct[0]['image'],true);

       $data =[];
       $data['image'] = $getArrImg[0];
       $data['price'] = $dataProduct[0]['price'];
       $data['name'] = $dataProduct[0]['name'];
       $data['id'] = $dataProduct[0]['id'];
       $data['qty'] = 1;
       // dd($data);
       $check = 1;
       $sum = 0 ;
        // echo "<pre>";
       if(session()->has('cart')){
       
            $getSession = session()->get('cart');
            // var_dump($getSession);
            foreach ($getSession as $key => $value) {
                if($id_product==$value['id']){
                    $getSession[$key]['qty'] +=1;
                    //session update
                    session()->put('cart',$getSession);
                    $check=2;
                }
            }
            
       }
       if($check==1){
            // them mang moi
           session()->push('cart',$data);
           
       }
       
       // $getSession1 = session()->get('cart');
       // echo 456;
       // var_dump($getSession1);
        $sum = 0 ;
       if(session()->has('cart')){
            $getSession = session()->get('cart');
            foreach ($getSession as $key => $value) {
                $sum = $sum + $getSession[$key]['qty'];
            }
            return response()->json([
                'tongQty' => $sum
            ]);
       }




    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
