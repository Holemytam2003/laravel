<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\AccountRequest;
use Auth;
class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd($user);
        return view('frontend/member/login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('frontend/member/register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterRequest $request)
    {
        $file = $request->avatar;
        $data = new User();
        // dd($request->all());
        $data->name= $request->name;
        $data->email=$request->email;
        $data->phone=$request->phone;
        $data->password=bcrypt($request->password);
        $data->address=$request->address;
        $data->avatar=$request->avatar->getClientOriginalName();
        $data->level=0;
        
        
       
        if($data->save()){

            if(!empty($file)){
                $file ->move('frontend/upload/user/avatar', $file->getClientOriginalName());
            }
              return redirect()->back()->with('success','Register success');
        }else{
             return redirect()->back()->withErrors('Register failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LoginRequest $request)
    {
        $login = [
            'email' =>$request->email,
            'password'=>$request->password,
            'level'=>0,
        ];
    
        if(Auth::attempt($login)){
            return redirect()->back()->with('success',' Login success');
        }else{
           return redirect()->back()->withErrors('Login failed');
        }
    }
    public function logout() {
          Auth::logout();
          return redirect('/member/login');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id = Auth::id();
        $data = User :: where('id',$id)->get()->toArray();
        $getData = $data[0];
        // dd($getData);
        return view('frontend/member/account',compact('getData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountRequest $request)
    {
        $user_id= auth::id();
        $user= User::findOrFail($user_id);

        $data = $request->all();
        // dd($data);
        $file= $request->avatar;
        // dd($file);
        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }

        if($data['password']){
            $data['password']= bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }

        if($user->update($data)){
            if(!empty($file)){
                $file->move('frontend/upload/user/avatar', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success','Update user success');
        }else{
            return redirect()->back()->withErrors('Update user failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
