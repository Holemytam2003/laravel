<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Rate;
use App\Models\Comment;
use Auth;

class BlogListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Blog::paginate(3);
        // dd($data);
        return view('frontend/blog/list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
    }
    public function ajax_rate(Request $request)
    {
        $getValue = $_POST['value'];
        $id_blog = $_POST['id_blog'];
        $id_user = Auth::id();
        $getAll = Rate:: where('id_user',$id_user )
                        -> where ('id_blog',$id_blog)
                        ->get()->toArray();
        
        // dd($getAll);
        if(!$getAll){
           $data = new Rate();
            $data->rate = $getValue;
            $data->id_user = $id_user;
            $data->id_blog = $id_blog;
            // dd($data);
            if($data->save()){
                return response()->json(['data'=>1]);
            }else{
                return response()->json(['data'=>2]);
            }
        }
       
    }
      public function comment(Request $request)
    {
            $data= new Comment();
            $data->id_user = Auth::id();
            $data->id_blog = $request->id;
            $data->cmt = $request->message;
            $data->name = Auth::user()->name;
            $data->level = $request->level;
            $data->avatar = Auth::user()->avatar;
            if($data->save()){
                return redirect()->back()->with('success','Comment success');
            }
           
            // return redirect('/blog/detail/{id}');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Blog::where('id',$id)->get()->toArray();
        $getData = $data[0];
        // dd($data);

        $next = Blog::where('id', '>', $id)->orderBy('id')->first();
        // dd($next);
        $previous = Blog::where('id', '<', $id)->orderBy('id','desc')->first();
        // dd($previous);
        
        // => xu ly rate 
        // tong diem danh gia / tong user
        $getAll=Rate::where('id_blog',$id)->get()->toArray();
        // echo "<pre>";
        // var_dump($getAll);
        $tbc = 0;
        // =>kiem tra da vote hay chua
        if(!empty($getAll)){
            $sum = 0 ;
            // tong user
            $user = count($getAll);
            foreach ($getAll as $key => $value) {
                $getRate = $value['rate'];
                // echo $getRate;

                // tong diem danh gia
                $sum = $sum + $getRate;
               

            }
            $tbc = round($sum/$user);
        }

        // =>xu ly comment
        $dataCmt = Comment::where('id_blog',$id)->get()->toArray();
      
        
       
    
        return view ('frontend/blog/detail',compact('getData','next','previous','tbc','dataCmt'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
