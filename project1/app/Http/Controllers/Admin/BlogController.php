<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;
use App\Models\Blog;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data= Blog::paginate(5);
        return view ('admin/blog/index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('admin/blog/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        // echo "<pre>";
        // dd($request->avatar);
        
        $data = new Blog();
        $data->title = $request->title;
        $data->image = $request->avatar->getClientOriginalName();
        $data->description= $request->description;
        $data->content= $request->content;
        $file = $request->avatar;
        // dd($file);
        // $data->save();
        // echo "<h1> Add success Click  <a href='/admin/blog'>here</a>to return blog page </h1>";
        if($data->save()){
            if(!empty($file)){
                $file ->move('admin/upload/blog/avatar', $file->getClientOriginalName());
            }
              return redirect('/admin/blog')->with('success','Add blog success');
        }else{
             return redirect()->back()->withErrors('Add blog failed');
        }
        
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data = Blog::where('id',$id)->get()->toArray();
        $edit = $data[0];
        return view('/admin/blog/edit',compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, $id)
    {
        // $update = Blog::where('id',$id)->update(
        //     [
        //         'title' => $request->title,
        //         'image' => $request->avatar,
                
        //     ]
        // );
        $data= Blog::findOrFail($id);
        $update = $request->all();
        $file = $request->avatar;
        $content = $request->content;
        // dd($content);
         // dd($update);
        if(!empty($file)){
            $data['image'] = $file->getClientOriginalName();
        }
        if(!empty($content)){
            $data['content'] = $content;
        }
        if($data->update($update)){
            if(!empty($file)){
                $file ->move('admin/upload/blog/avatar', $file->getClientOriginalName());
            }
            return redirect('/admin/blog')->with('success','Upload blog success');
        } else{
            return redirect()->back()->withErrors('Update blog failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         
         Blog::where('id',$id)->delete();
        echo "<h1> Delete success Click  <a href='/admin/blog'>đây</a> để về trang danh sách</h1>";
    }
}
