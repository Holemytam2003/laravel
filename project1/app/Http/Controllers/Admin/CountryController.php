<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AddCountryRequest;
use App\Models\Country;

class CountryController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data = Country::all()->toArray();
        return view('admin/country/country',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
         // $data=Index::all()->toArray();
       return view('admin/country/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCountryRequest $request)
    {
          //   echo "<h1> Add thông tin cầu thủ thành công Click vào <a href='model'>đây</a> để về trang danh sách</h1>";
         $data = new Country();
         $data->name = $request->name;
         $data->save();
          echo "<h1> Add success Click  <a href='/admin/country'>đây</a> để về trang danh sách</h1>";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = Country::where('id',$id)->get()->toArray();
         return view('admin/country/edit',compact('data'));
        // dd($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddCountryRequest $request, $id)
    {
        // dd($request->all());
        $update = Country::where('id',$id)->update(
            [
                'name' => $request->name,
            ]
        );
        echo "<h1> Edit success Click  <a href='/admin/country'>đây</a> để về trang danh sách</h1>";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Country::where('id',$id)->delete();
        echo "<h1> Delete success Click  <a href='/admin/country'>đây</a> để về trang danh sách</h1>";
         // Index::where('id',$id)->delete();
    }
}
