<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfileRequest;
use Auth;
use App\Models\User;
use App\Models\Country;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $data = Country::all()->toArray();
        // dd($data);
        return view('admin/user/profile',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request)
    {
        // lay id user
        $user_id= auth::id();
        // findOrFail the same as select... where id = id
        $user= User::findOrFail($user_id);
        // echo $user->password;
        // dd($user);
        // lay all thong tin tu form nhap vao
        $data = $request->all();
        // dd($data);
        $file= $request->avatar;

        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }

        if($data['password']){
            $data['password']= bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }
      
        if($user->update($data)){
            if(!empty($file)){
                $file ->move('admin/upload/user/avatar', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success','Upload proile success');
        } else{
            return redirect()->back()->withErrors('Update profile failed');
        }
    
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
